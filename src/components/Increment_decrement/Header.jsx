import React, { Component } from 'react'
import '../../styles/header.scss'

class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            count: 0
        }
    }

    increment() {
        this.setState(prevState => ({
            count: prevState.count + 1
        }))
    }

    decrement() {
        if (this.state.count > 0) {
            this.setState(prevState => ({
                count: prevState.count - 1
            }))
        }
    }

    reset() {
        this.setState({
            count: 0
        })
    }

    render() {
        return (
            <div className="container">
                <div className="box">
                    <button id="btn1" onClick={() => this.decrement()}><span>-</span></button>
                    <input type="text" className="input" id="num" value={this.state.count} />
                    <button id="btn2" onClick={() => this.increment()}><span>+</span></button>
                </div>
                <button id="btn3" onClick={() => this.reset()}>Reset</button>
            </div>

        )
    }
}

export default Header
