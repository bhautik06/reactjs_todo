import React from "react";
import { AiFillDelete } from "react-icons/ai";
import "../../styles/todolist.scss";

export default function Todo({ todo, toggleTodo }) {
  const handleTodoClick = () => {
    toggleTodo(todo.id);
  };

  return (
    <label>
        <div className="task">
          <input                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
            type="checkbox"
            checked={todo.complete}
            onChange={handleTodoClick}
          />
          <spna>{todo.name}</spna>
          <button id="deletebtn">
            <AiFillDelete />
          </button>
        </div>
    </label>
  );
}
