import React, { useState, useRef, useEffect } from "react";
import TodoData from "./ToDoData";
import { v4 as uuidv4 } from "uuid";
import "../../styles/todolist.scss";

const LOCAL_STORAGE_KEY = "todoApp.todos";

function ToDoList() {
  const [todos, setTodos] = useState([]);
  const todoNameRef = useRef();

  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if (storedTodos) setTodos(storedTodos);
  }, []);

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos));
  }, [todos]);

  function toggleTodo(id) {
    const newTodos = [...todos];
    const todo = newTodos.find((todo) => todo.id === id);
    todo.complete = !todo.complete;
    setTodos(newTodos);
  }

  function handleAddTodo(e) {
    const name = todoNameRef.current.value;
    if (name === "") return;
    setTodos((prevTodos) => {
      return [...prevTodos, { id: uuidv4(), name: name, complete: false }];
    });
    todoNameRef.current.value = null;
  }

  function handleClearTodos() {
    const newTodos = todos.filter((todo) => !todo.complete);
    setTodos(newTodos);
  }

  var weekdays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  var dateObj = new Date();
  var weekdayNumber = dateObj.getDay();
  var weekdayName = weekdays[weekdayNumber];

  return (
    <div>
      <div className="container">
        <p className="dates">
          {weekdayName},<br />
          {new Date().toLocaleDateString()}
        </p>
        <div className="addTodos">
          <input ref={todoNameRef} type="text" placeholder="Take the garbage out" />
          <button id="addToDo" onClick={handleAddTodo}>
            +
          </button>
        </div>
        <div className="leftTodos">
          You have {todos.filter((todo) => !todo.complete).length} pendings
          items.
        </div>
        <div className="todolist">
          <TodoData todos={todos} toggleTodo={toggleTodo} />
        </div>
        <button onClick={handleClearTodos}>Clear Complete</button>
      </div>
    </div>
  );
}

export default ToDoList;
