import React from 'react';
import './App.css';
// import Header from './components/Increment_decrement/Header';
import ToDoList from './components/ToDo/ToDoList';

function App() {
  return (
    <div className="App">
        <ToDoList />
        {/* <Header /> */}
    </div>
  );
}

export default App;
